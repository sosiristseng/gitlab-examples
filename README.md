# GitLab Templates

My GitLab CI/CD pipeline templates and examples.

## Hugo

```yml
include:
  - project: 'sosiristseng/gitlab-templates'
    file: '/hugo.gitlab-ci.yml'
```

## Hexo

```yml
include:
  - project: 'sosiristseng/gitlab-templates'
    file: '/hexo.gitlab-ci.yml'
```

## Docsify

```yml
include:
  - project: 'sosiristseng/gitlab-templates'
    file: '/docsify.gitlab-ci.yml'
```

## Others

Please checkout the respective `.gitlab-ci.yml` files.
